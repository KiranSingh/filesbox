using System.Threading.Tasks;
using FilesBox.Api.Models;
using FilesBox.Api.Validators;
using FluentAssertions;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Moq;
using Xunit;

namespace Api.UnitTests.Validators
{
    public class FileUploadModelValidatorTests
    {
        private readonly FileUploadModelValidator _validator;

        private readonly FileConfig _fileConfig = new()
        {
            BaseUrl = "https://localhost:5001/api",
            MaxSizeAllowed = 102400,
        };

        private readonly FileUploadModel _fileUploadModel;
        private Mock<IFormFile> _formFile;

        public FileUploadModelValidatorTests()
        {
            _formFile = new Mock<IFormFile>();
            _formFile.SetupGet(x => x.ContentType)
                .Returns("application/pdf");
            _formFile.SetupGet(x => x.Length)
                .Returns(_fileConfig.MaxSizeAllowed - 100);
            _fileUploadModel = new FileUploadModel
            {
                File = _formFile.Object,
            };
            _validator = new FileUploadModelValidator(_fileConfig);
        }

        [Fact]
        public void Class_Inherits_ValidatorBase()
        {
            // Arrange // Act // Assert
            typeof(FileUploadModelValidator)
                .Should().BeAssignableTo<AbstractValidator<FileUploadModel>>();
        }

        [Fact]
        public async Task ValidateAsync_ValidModel_ReturnsIsValidTrue()
        {
            // Arrange // Act
            var actual = await _validator.ValidateAsync(_fileUploadModel);

            // Assert
            _formFile.VerifyGet(x => x.ContentType);
            _formFile.VerifyGet(x => x.Length);
            actual.IsValid.Should().BeTrue();
        }

        [Fact]
        public async Task ValidateAsync_ContentTypeInvalid_ReturnsIsValidFalse()
        {
            // Arrange
            _formFile.SetupGet(x => x.ContentType)
                .Returns("text/plain");
            
            // Act
            var actual = await _validator.ValidateAsync(_fileUploadModel);

            // Assert
            actual.IsValid.Should().BeFalse();
            actual.ToString().Should().Be("Only PDF file can be uploaded");
        }
        
        [Fact]
        public async Task ValidateAsync_FileSizeMoreThanMaxAllowed_ReturnsIsValidFalse()
        {
            // Arrange
            _formFile.SetupGet(x => x.Length)
                .Returns(_fileConfig.MaxSizeAllowed + 20);
            
            // Act
            var actual = await _validator.ValidateAsync(_fileUploadModel);

            // Assert
            actual.IsValid.Should().BeFalse();
            actual.ToString().Should().Be($"A max file size of {_fileConfig.MaxSize()} is allowed");
        }
        
        [Theory]
        [InlineData(0)]
        [InlineData(-7)]
        public async Task ValidateAsync_FileSizeNotValid_ReturnsIsValidFalse(long invalid)
        {
            // Arrange
            _formFile.SetupGet(x => x.Length)
                .Returns(invalid);
            
            // Act
            var actual = await _validator.ValidateAsync(_fileUploadModel);

            // Assert
            actual.IsValid.Should().BeFalse();
            actual.ToString().Should().Be("A valid file is required for this call");
        }
        
        [Fact]
        public async Task ValidateAsync_FileIsNull_ReturnsIsValidFalse()
        {
            // Arrange
            _fileUploadModel.File = null;
            
            // Act
            var actual = await _validator.ValidateAsync(_fileUploadModel);

            // Assert
            actual.IsValid.Should().BeFalse();
            actual.ToString().Should().Be("A valid file is required for this call");
        }

        [Fact]
        public async Task ValidateAsync_ModelIsNull_ReturnsIsValidFalse()
        {
            // Arrange // Act
            var actual = await _validator.ValidateAsync(default(FileUploadModel));

            // Assert
            actual.IsValid.Should().BeFalse();
            actual.ToString().Should().Be($"A valid {nameof(FileUploadModel)} is required for this call");
        }
    }
}