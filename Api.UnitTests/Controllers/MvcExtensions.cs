using FluentAssertions;
using Microsoft.AspNetCore.Mvc;

namespace Api.UnitTests.Controllers
{
    public static class MvcExtensions
    {
        public static string ModelStateErrorValue(this IActionResult actionResult, string key) =>
            actionResult.As<BadRequestObjectResult>()
                .Value.As<SerializableError>()[key].As<string[]>()[0];
    }
}