using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using FilesBox.Api.Controllers;
using FilesBox.Api.Models;
using FilesBox.Api.Services;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Api.UnitTests.Controllers
{
    public class FileControllerTests
    {
        private readonly FileController _controller;
        private readonly Mock<IFileService> _fileService;

        public FileControllerTests()
        {
            _fileService = new Mock<IFileService>();
            _controller = new FileController(_fileService.Object);
        }

        [Fact]
        public void Class_Inherits_Controller()
        {
            // Arrange // Act // Assert
            typeof(FileController)
                .Should().BeAssignableTo<Controller>();
        }

        [Fact]
        public void Class_RouteAttribute_ExpectedTemplate()
        {
            // Arrange // Act // Assert
            typeof(FileController)
                .Should().BeDecoratedWith<RouteAttribute>(x => x.Template == "api/[controller]");
        }
        
        [Fact]
        public void Get_HttpGetAttribute_ExpectedTemplate()
        {
            // Arrange // Act // Assert
            typeof(FileController)
                .GetMethod(nameof(FileController.Get))
                .Should().BeDecoratedWith<HttpGetAttribute>(x => x.Template == "{id}");
        }

        [AutoData, Theory]
        public async Task Get_ValidId_ReturnsFileResult(string id, BoxFile boxFile)
        {
            // Arrange
            boxFile.Content = await File.ReadAllBytesAsync("../../../TestFile.txt");
            boxFile.ContentType = "text/plain";
            _fileService.Setup(x => x.GetAsync(id))
                .ReturnsAsync(boxFile);

            // Act
            var actual = (await _controller.Get(id)).As<FileStreamResult>();

            // Assert
            _fileService.Verify(x => x.GetAsync(id));
            actual.Should().NotBeNull();
            actual.ContentType.Should().Be(boxFile.ContentType);
            actual.FileDownloadName.Should().Be(boxFile.Name);
        }

        [AutoData, Theory]
        public async Task Get_InvalidId_ReturnsBadRequestResult(string id)
        {
            // Arrange
            _fileService.Setup(x => x.GetAsync(id))
                .ReturnsAsync(default(BoxFile));

            // Act
            var actual = (await _controller.Get(id)).As<BadRequestObjectResult>();

            // Assert
            _fileService.Verify(x => x.GetAsync(id));
            actual.Should().NotBeNull();
            actual.Value.Should().Be($"No valid file for Id: '{id}'");
        }

        [Fact]
        public void Post_HttpAttribute_Present()
        {
            // Arrange // Act // Assert
            typeof(FileController)
                .GetMethod(nameof(FileController.Post))
                .Should().BeDecoratedWith<HttpPostAttribute>();
        }

        [Fact]
        public void Post_Parameter_HasFromFormAttribute()
        {
            // Arrange // Act // Assert
            typeof(FileController)
                .GetMethod(nameof(FileController.Post))
                .GetParameters().Single()
                .GetCustomAttribute<FromFormAttribute>()
                .Should().NotBeNull();
        }
        
        [AutoData, Theory]
        public async Task Post_ModelStateValid_CallsSaveOnService_ReturnsOkResult(Mock<FileUploadModel> model)
        {
            // Arrange // Act
            var actual = await _controller.Post(model.Object);

            // Assert
            _fileService.Verify(x => x.SaveAsync(model.Object));
            actual.As<OkResult>().Should().NotBeNull();
        }

        [AutoData, Theory]
        public async Task Post_ModelStateInvalid_ReturnsBadRequest(Mock<FileUploadModel> model, string expected)
        {
            // Arrange
            _controller.ModelState.AddModelError("someModel", expected);

            // Act
            var actual = await _controller.Post(model.Object);

            // Assert
            _fileService.Verify(x => x.SaveAsync(It.IsAny<FileUploadModel>()), Times.Never);
            actual.ModelStateErrorValue("someModel").Should().Be(expected);
        }
    }
}