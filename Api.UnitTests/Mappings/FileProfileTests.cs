using AutoFixture.Xunit2;
using AutoMapper;
using FilesBox.Api.Mappings;
using FilesBox.Api.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Moq;
using Xunit;

namespace Api.UnitTests.Mappings
{
    public class FileProfileTests
    {
        private readonly IMapper _mapper;
        private readonly MapperConfiguration _mapperConfiguration;

        public FileProfileTests()
        {
            _mapperConfiguration = new MapperConfiguration(cfg => cfg.AddProfile<FileProfile>());
            _mapper = _mapperConfiguration.CreateMapper();
        }
        
        [Fact]
        public void Mappings_Configuration_Valid()
        {
            // Arrange // Act // Assert
            _mapperConfiguration.AssertConfigurationIsValid();
        }

        [AutoData, Theory]
        public void Map_FileUploadModelToBoxFile_ResultAsExpected(byte[] content, string contentType, string fileName)
        {
            // Arrange
            var formFile = new Mock<IFormFile>();
            formFile.SetupGet(x => x.ContentType)
                .Returns(contentType);
            formFile.SetupGet(x => x.FileName)
                .Returns(fileName);
            var fileUploadModel = new Mock<FileUploadModel>{CallBase = true};
            fileUploadModel.Setup(x => x.Content())
                .Returns(content);
            fileUploadModel.Object.File = formFile.Object;

            // Act
            var actual = _mapper.Map<FileUploadModel, BoxFile>(fileUploadModel.Object);

            // Assert
            fileUploadModel.Setup(x => x.Content());
            formFile.VerifyGet(x => x.ContentType);
            formFile.VerifyGet(x => x.FileName);
            actual.Content.Should().BeEquivalentTo(content);
            actual.ContentType.Should().Be(contentType);
            actual.Name.Should().Be(fileName);
        }
    }
}