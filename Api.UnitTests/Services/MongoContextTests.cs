using FilesBox.Api.Models;
using FilesBox.Api.Services;
using FluentAssertions;
using MongoDB.Driver;
using Moq;
using Xunit;

namespace Api.UnitTests.Services
{
    public class MongoContextTests
    {
        private readonly MongoContext _mongoContext;
        private readonly Mock<IMongoDatabase> _mongoDatabase;

        public MongoContextTests()
        {
            _mongoDatabase = new Mock<IMongoDatabase>();
            _mongoContext = new MongoContext(_mongoDatabase.Object);
        }

        [Fact]
        public void Class_Implements_IMongoContext()
        {
            // Arrange // Act // Assert
            typeof(MongoContext)
                .Should().BeAssignableTo<IMongoContext>();
        }

        [Fact]
        public void Collection_ForBoxFile_ReturnsBoxFilesCollection()
        {
            // Arrange
            var expected = new Mock<IMongoCollection<BoxFile>>().Object;
            _mongoDatabase.Setup(x => x.GetCollection<BoxFile>($"{nameof(BoxFile)}s", null))
                .Returns(expected);

            // Act
            var actual = _mongoContext.Collection<BoxFile>();

            // Assert
            _mongoDatabase.Verify(x => x.GetCollection<BoxFile>($"{nameof(BoxFile)}s", null));
            actual.Should().Be(expected);
        }

        [Fact]
        public void Collection_ForFileOrder_ReturnsFileOrdersCollection()
        {
            // Arrange
            var expected = new Mock<IMongoCollection<FileOrder>>().Object;
            _mongoDatabase.Setup(x => x.GetCollection<FileOrder>($"{nameof(FileOrder)}s", null))
                .Returns(expected);

            // Act
            var actual = _mongoContext.Collection<FileOrder>();

            // Assert
            _mongoDatabase.Verify(x => x.GetCollection<FileOrder>($"{nameof(FileOrder)}s", null));
            actual.Should().Be(expected);
        }
    }
}