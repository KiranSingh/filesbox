using System.Collections.Generic;
using AutoFixture.Xunit2;
using FilesBox.Api.Models;
using FluentAssertions;
using Xunit;

namespace Api.UnitTests.Models
{
    public class BoxFileTests
    {
        [AutoData, Theory]
        public void Valid_PropertiesValid_ReturnsTrue(BoxFile boxFile)
        {
            // Arrange // Act
            var actual = boxFile.Valid();

            // Assert
            actual.Should().BeTrue();
        }

        [AutoData, Theory]
        public void Valid_NameIsNotValid_ReturnsFalse(BoxFile boxFile)
        {
            // Arrange
            new List<string>{null, "", "    "}
                .ForEach(invalid =>
                {
                    boxFile.Name = invalid;
                    
                    // Act
                    var actual = boxFile.Valid();

                    // Assert
                    actual.Should().BeFalse();
                });
        }

        [AutoData, Theory]
        public void Valid_ContentIsEmpty_ReturnsFalse(BoxFile boxFile)
        {
            // Arrange
            boxFile.Content = new byte[]{};
                    
            // Act
            var actual = boxFile.Valid();

            // Assert
            actual.Should().BeFalse();
        }

        [AutoData, Theory]
        public void Valid_ContentIsNull_ReturnsFalse(BoxFile boxFile)
        {
            // Arrange
            boxFile.Content = null;
                    
            // Act
            var actual = boxFile.Valid();

            // Assert
            actual.Should().BeFalse();
        }
    }
}