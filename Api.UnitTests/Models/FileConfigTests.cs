using AutoFixture.Xunit2;
using FilesBox.Api.Extensions;
using FilesBox.Api.Models;
using FluentAssertions;
using Xunit;

namespace Api.UnitTests.Models
{
    public class FileConfigTests
    {
        [AutoData, Theory]
        public void MaxSize_StandardCall_ResultAsExpected(FileConfig fileConfig)
        {
            // Arrange // Act
            var actual = fileConfig.MaxSize();

            // Assert
            actual.Should().Be(fileConfig.MaxSizeAllowed.SizeSuffix());
        }
    }
}