using System;
using AutoMapper;
using FilesBox.Api;
using FilesBox.Api.Models;
using FilesBox.Api.Services;
using FilesBox.Api.Validators;
using FluentAssertions;
using FluentValidation;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver;
using Xunit;

namespace Api.UnitTests
{
    public class StartupTests
    {
        [Theory]
        [InlineData(typeof(FileConfig))]
        [InlineData(typeof(MongoClient))]
        [InlineData(typeof(IMongoDatabase))]
        public void GetRequiredService_ValidType_ReturnsNotNull(Type expectedType)
        {
            var webHost = Microsoft.AspNetCore.WebHost.CreateDefaultBuilder().UseStartup<Startup>().Build();

            webHost.Services.GetRequiredService(expectedType).Should().NotBeNull();
        }
        
        [Theory]
        [InlineData(typeof(IFileService), typeof(FileService))]
        [InlineData(typeof(IMapper), typeof(Mapper))]
        [InlineData(typeof(IMongoContext), typeof(MongoContext))]
        [InlineData(typeof(IValidator<FileUploadModel>), typeof(FileUploadModelValidator))]
        public void GetRequiredService_ValidInterface_HasExpectedType(Type requiredInterface, Type expectedType)
        {
            var webHost = Microsoft.AspNetCore.WebHost.CreateDefaultBuilder().UseStartup<Startup>().Build();

            webHost.Services.GetRequiredService(requiredInterface).Should().BeOfType(expectedType);
        }
    }
}