using System;
using FilesBox.Api.Models;
using Mongo2Go;
using MongoDB.Driver;

namespace Api.IntegrationTests.Services
{
    public class MongoTestFixture : IDisposable
    {
        private readonly MongoDbRunner _runner;

        public MongoTestFixture()
        {
            _runner = MongoDbRunner.Start(singleNodeReplSet: false);
            var client = new MongoClient(_runner.ConnectionString);
            Database = client.GetDatabase("IntegrationTest");
            
            BoxFiles = Database.GetCollection<BoxFile>(nameof(BoxFiles));
            FileOrders = Database.GetCollection<FileOrder>(nameof(FileOrders));
        }
        
        public IMongoCollection<BoxFile> BoxFiles { get; }
        
        public IMongoDatabase Database { get; }
        
        public IMongoCollection<FileOrder> FileOrders { get; }
        
        public void Dispose() => _runner?.Dispose();
    }
}