using System;
using System.IO;
using System.Threading.Tasks;
using AutoMapper;
using FilesBox.Api.Mappings;
using FilesBox.Api.Models;
using FilesBox.Api.Services;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using MongoDB.Driver;
using Xunit;

namespace Api.IntegrationTests.Services
{
    public class FileServiceTests : IClassFixture<MongoTestFixture>
    {
        private const string FormatFilePath = "../../../Files/{0}";
        private const string KeyApplicationPdf = "application/pdf";
        private const string KeyFile01Name = "redis.pdf";
        private const string KeyFile02Name = "rules.pdf";

        private readonly IMongoCollection<BoxFile> _boxFilesCollection;
        private readonly IMongoCollection<FileOrder> _fileOrdersCollection;
        private readonly FileService _fileService;

        public FileServiceTests(MongoTestFixture testFixture)
        {
            _boxFilesCollection = testFixture.BoxFiles;
            _boxFilesCollection.DeleteMany(FilterDefinition<BoxFile>.Empty);

            _fileOrdersCollection = testFixture.FileOrders;
            _fileOrdersCollection.DeleteMany(FilterDefinition<FileOrder>.Empty);

            _fileService = new FileService(
                new MapperConfiguration(cfg => cfg.AddProfile<FileProfile>()).CreateMapper(),
                new MongoContext(testFixture.Database));
        }

        [Fact]
        public async Task GetAsync_ValidId_ReturnsExpectedBoxFile()
        {
            // Arrange
            var boxFile = new BoxFile
            {
                Content = await File.ReadAllBytesAsync(string.Format(FormatFilePath, KeyFile01Name)),
                ContentType = KeyApplicationPdf,
                Name = "redis.pdf",
            };
            await _boxFilesCollection.InsertOneAsync(boxFile);

            // Act
            var actual = await _fileService.GetAsync(KeyFile01Name);

            // Assert
            actual.Should().BeEquivalentTo(boxFile);
        }

        [Fact]
        public async Task GetAsync_InvalidId_ReturnsNull()
        {
            // Arrange // Act
            var actual = await _fileService.GetAsync("invalid");

            // Assert
            actual.Should().BeNull();
        }

        [Fact]
        public async Task Post_ValidModel_AddsExpectedItemsToCollections()
        {
            // Arrange
            await using (var fileStream = File.OpenRead(string.Format(FormatFilePath, KeyFile02Name)))
            {
                var length = fileStream.Length;
                var formFile = new FormFile(fileStream, 0, length, "File", KeyFile02Name)
                {
                    Headers = new HeaderDictionary {{"Content-Type", KeyApplicationPdf}},
                };
                var model = new FileUploadModel {File = formFile};

                // Act
                await _fileService.SaveAsync(model);
            }

            // Assert
            var expectedBytes = await File.ReadAllBytesAsync(string.Format(FormatFilePath, KeyFile02Name));
            var boxFile = await _boxFilesCollection.Find(x => x.Name == KeyFile02Name).SingleOrDefaultAsync();
            boxFile.Content.Should().BeEquivalentTo(expectedBytes);
            boxFile.ContentType.Should().Be(KeyApplicationPdf);
            boxFile.Name.Should().Be(KeyFile02Name);

            var fileOrder = await _fileOrdersCollection.Find(x => x.Id == nameof(FileOrder)).SingleOrDefaultAsync();
            fileOrder.FileNames.Should().Contain(KeyFile02Name);
        }

        [Fact]
        public async Task Post_ModeLNull_ThrowsException()
        {
            // Arrange // Act
            var actual =
                await Assert.ThrowsAsync<ArgumentNullException>(() => _fileService.SaveAsync(default(FileUploadModel)));

            // Assert
            actual.ParamName.Should().Be(nameof(FileUploadModel));
        }
    }
}