using FluentValidation;
using FluentValidation.Results;

namespace FilesBox.Api.Validators
{
    public class ValidatorBase<T> : AbstractValidator<T>
    {
        public const string FormatErrorMessagePropertyRequired = "A valid {0} is required for this call";

        protected override bool PreValidate(ValidationContext<T> context, ValidationResult result)
        {
            if (context.InstanceToValidate != null) 
                return true;
            
            result.Errors.Add(new ValidationFailure("", string.Format(FormatErrorMessagePropertyRequired, typeof(T).Name)));
            return false;
        }
    }
}