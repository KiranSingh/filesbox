using FilesBox.Api.Models;
using FluentValidation;

namespace FilesBox.Api.Validators
{
    public class FileUploadModelValidator : ValidatorBase<FileUploadModel>
    {
        private const string ErrorMessageValidFileRequired = "A valid file is required for this call";

        public FileUploadModelValidator(FileConfig fileConfig)
        {
            RuleFor(x => x.File)
                .Cascade(CascadeMode.Stop)
                .NotNull()
                .WithMessage(ErrorMessageValidFileRequired)
                .Must(x => x.Length > 0)
                .WithMessage(ErrorMessageValidFileRequired)
                .Must(x => x.ContentType.ToLower().Contains("pdf"))
                .WithMessage("Only PDF file can be uploaded")
                .Must(x => x.Length <= fileConfig.MaxSizeAllowed)
                .WithMessage(x => $"A max file size of {fileConfig.MaxSize()} is allowed");
        }
    }
}