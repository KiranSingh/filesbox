using System.Threading.Tasks;
using AutoMapper;
using FilesBox.Api.Extensions;
using FilesBox.Api.Models;
using MongoDB.Driver;

namespace FilesBox.Api.Services
{
    public class FileService : IFileService
    {
        private readonly IMapper _mapper;
        private readonly IMongoCollection<BoxFile> _boxFilesCollection;
        private readonly IMongoCollection<FileOrder> _fileOrdersCollection;

        public FileService(IMapper mapper, IMongoContext mongoContext)
        {
            _mapper = mapper;
            _boxFilesCollection = mongoContext.Collection<BoxFile>();
            _fileOrdersCollection = mongoContext.Collection<FileOrder>();
        }
        
        public async Task<BoxFile> GetAsync(string id)
        {
            var file = await _boxFilesCollection.Find(x => x.Name == id).FirstOrDefaultAsync();

            return file == null || !file.Valid() ? null : file;
        }

        public async Task SaveAsync(FileUploadModel model)
        {
            model.ValidateNotNull(nameof(FileUploadModel));
            
            var file = _mapper.Map<BoxFile>(model);

            await _boxFilesCollection.UpdateOneAsync(x => x.Name == file.Name, file.UpdateDefinition(),
                new UpdateOptions {IsUpsert = true});

            await _fileOrdersCollection.UpdateOneAsync(x => x.Id == nameof(FileOrder),
                Builders<FileOrder>.Update.AddToSet(x => x.FileNames, file.Name),
                new UpdateOptions {IsUpsert = true,});
        }
    }
}