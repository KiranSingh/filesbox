using System.Threading.Tasks;
using FilesBox.Api.Models;

namespace FilesBox.Api.Services
{
    public interface IFileService
    {
        Task<BoxFile> GetAsync(string id);
        
        Task SaveAsync(FileUploadModel model);
    }
}