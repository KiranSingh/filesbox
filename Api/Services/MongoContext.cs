using MongoDB.Driver;

namespace FilesBox.Api.Services
{
    public class MongoContext : IMongoContext
    {
        private readonly IMongoDatabase _database;

        public MongoContext(IMongoDatabase mongoDatabase) => _database = mongoDatabase;

        public IMongoCollection<T> Collection<T>() =>
            _database.GetCollection<T>($"{typeof(T).Name}s");
    }
}