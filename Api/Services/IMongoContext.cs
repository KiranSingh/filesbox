using MongoDB.Driver;

namespace FilesBox.Api.Services
{
    public interface IMongoContext
    {
        IMongoCollection<T> Collection<T>();
    }
}