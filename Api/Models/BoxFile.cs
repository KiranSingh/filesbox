using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace FilesBox.Api.Models
{
    public class BoxFile
    {
        public byte[] Content { get; set; }

        public string ContentType { get; set; }
        
        [BsonId] 
        public string Name { get; set; }

        public UpdateDefinition<BoxFile> UpdateDefinition()
        {
            var updates = new List<UpdateDefinition<BoxFile>>();
            
            if(Content != null && Content.Any())
                updates.Add(Builders<BoxFile>.Update.Set(x => x.Content, Content));
            
            if(!string.IsNullOrWhiteSpace(ContentType))
                updates.Add(Builders<BoxFile>.Update.Set(x => x.ContentType, ContentType));

            return Builders<BoxFile>.Update.Combine(updates);
        }

        public bool Valid() => !string.IsNullOrWhiteSpace(Name) && Content != null && Content.Any();
    }
}