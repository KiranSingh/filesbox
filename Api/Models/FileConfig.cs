using FilesBox.Api.Extensions;

namespace FilesBox.Api.Models
{
    public class FileConfig
    {
        public string BaseUrl { get; set; }

        public long MaxSizeAllowed { get; set; }

        public string MaxSize() => MaxSizeAllowed.SizeSuffix();
    }
}