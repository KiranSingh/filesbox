using MongoDB.Bson.Serialization.Attributes;

namespace FilesBox.Api.Models
{
    public class FileOrder
    {
        [BsonId]
        public string Id { get; set; }
        
        public string[] FileNames { get; set; }
    }
}