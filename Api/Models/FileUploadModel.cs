using System.IO;
using Microsoft.AspNetCore.Http;

namespace FilesBox.Api.Models
{
    public class FileUploadModel
    {
        public IFormFile File { get; set; }
        
        public virtual byte[] Content()
        {
            using var ms = new MemoryStream();
            File.CopyTo(ms);
            return ms.ToArray();
        }
    }
}