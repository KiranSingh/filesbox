using MongoDB.Bson;
using MongoDB.Driver;

namespace FilesBox.Api.Extensions
{
    public static class MongoDatabaseExtensions
    {
        public static bool CollectionExists(this IMongoDatabase database, string collectionName)
        {
            var filter = new BsonDocument("name", collectionName);
            var options = new ListCollectionNamesOptions { Filter = filter };

            return database.ListCollectionNames(options).Any();
        }
    }
}