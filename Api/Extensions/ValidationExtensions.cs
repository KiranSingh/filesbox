using System;

namespace FilesBox.Api.Extensions
{
    public static class ValidationExtensions
    {
        public static void ValidateNotNull<T>(this T item, string name) where T : class
        {
            if (item == null)
                throw new ArgumentNullException(name);
        }
    }
}