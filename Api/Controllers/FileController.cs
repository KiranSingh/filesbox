using System.IO;
using System.Threading.Tasks;
using FilesBox.Api.Models;
using FilesBox.Api.Services;
using Microsoft.AspNetCore.Mvc;

namespace FilesBox.Api.Controllers
{
    [Route("api/[controller]")]
    public class FileController : Controller
    {
        private readonly IFileService _fileService;

        public FileController(IFileService fileService)
        {
            _fileService = fileService;
        }
        
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(string id)
        {
            var file = await _fileService.GetAsync(id);

            if (file == null)
                return BadRequest($"No valid file for Id: '{id}'");

            return File(new MemoryStream(file.Content), file.ContentType, file.Name);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromForm] FileUploadModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            await _fileService.SaveAsync(model);

            return Ok();
        }
    }
}