using System;
using System.Net;
using AutoMapper;
using FilesBox.Api.Extensions;
using FilesBox.Api.Models;
using FilesBox.Api.Services;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using MongoDB.Bson;
using MongoDB.Driver;

namespace FilesBox.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var fileConfig = new FileConfig();
            Configuration.GetSection("FileConfig").Bind(fileConfig);
            services.AddSingleton(fileConfig);
            
            var mongoConnectionString = Configuration.GetConnectionString("FilesBox");
            
            services.AddSingleton<MongoClient>(sp => new MongoClient(mongoConnectionString));
            
            services.AddScoped<IMongoDatabase>(sp =>
            {
                var mongoUrl = MongoUrl.Create(mongoConnectionString);

                var mongoDatabase = sp.GetService<MongoClient>()
                    .GetDatabase(mongoUrl.DatabaseName,
                        new MongoDatabaseSettings
                        {
                            GuidRepresentation = GuidRepresentation.Standard
                        });
                
                // Secondary collation strength creates case insensitive indexes in MongoDB
                // which allows case insensitive search
                var boxFilesCollectionName = $"{nameof(BoxFile)}s";
                if (!mongoDatabase.CollectionExists(boxFilesCollectionName))
                {
                    mongoDatabase.CreateCollection(boxFilesCollectionName, new CreateCollectionOptions
                    {
                        Collation = new Collation("en", strength: CollationStrength.Secondary),
                    });
                }

                return mongoDatabase;
            });

            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IMongoContext, MongoContext>();

            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            services
                .AddRouting(opt =>
                {
                    opt.LowercaseUrls = true;
                    opt.LowercaseQueryStrings = true;
                })
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());
            
            services.AddControllers();
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo {Title = "Api", Version = "v1"}); });        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logger, IMapper mapper)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Api v1"));
            }
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseExceptionHandler(
                builder =>
                {
                    builder.Run(
                        async context =>
                        {
                            context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                            context.Response.Headers.Add("Access-Control-Allow-Origin", "*");

                            var error = context.Features.Get<IExceptionHandlerFeature>();
                            if (error != null)
                            {
                                var errorMessage = error.Error.Message;
                                if (error.Error.InnerException is { } exception)
                                    errorMessage += $" Inner Exception Message: {exception.Message}";
                                logger.LogError(error.Error, errorMessage);

                                context.Response.Headers.Add("Application-Error", errorMessage);
                                // CORS
                                context.Response.Headers.Add("access-control-expose-headers", "Application-Error");
                                await context.Response.WriteAsync(errorMessage).ConfigureAwait(false);
                            }
                        });
                });

            mapper.ConfigurationProvider.AssertConfigurationIsValid();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}