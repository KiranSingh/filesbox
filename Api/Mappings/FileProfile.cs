using AutoMapper;
using FilesBox.Api.Models;

namespace FilesBox.Api.Mappings
{
    public class FileProfile : Profile
    {
        public FileProfile()
        {
            CreateMap<FileUploadModel, BoxFile>()
                .ForMember(dest => dest.Content, opt => opt.MapFrom(src => src.Content()))
                .ForMember(dest => dest.ContentType,
                    opt => opt.MapFrom(src => src.File == null ? null : src.File.ContentType))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.File == null ? null : src.File.FileName));
        }
    }
}