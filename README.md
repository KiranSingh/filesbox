# Files Box

Repository for Files Box API

## Prerequisites

Install:

* Dotnet Core 5.0 SDK
* MongoDB

## Getting started

The application requires a running instance of **MongoDB** (to be containerised in the next version). If *MongoDB* is not running on the default URL (*localhost:27017*) the connection string **FilesBox** should be updated in *appsettings.json*.

The application runs on default port (5001) and provides a [Swagger](https://localhost:5001/swagger) page. This port can be changed by updating the *applicationUrl* for the *Api* profile in *launchSettings.json*.

### Operation
After start, when a document is *POST*ed via the *api/file* endpoint, the *FilesBox* database with *BoxFiles* and *FileOrders* collections would be created in *MongoDB*. 
The *api/file* *GET* endpoint with a valid file name would return the posted file.

At present only *PDF* files of upto *5MB* are allowed to be uploaded.

Any subsequent file uploaded would be stored in order and those would be returned in that order on calling the *api/files* *GET* endpoint. *POST*ing a complete list of files in another order to that endpoint will change the order for any subsequent calls to the *GET* endpoint.

### Logging
No logging has been setup due to time constraints.

## Note:
* At present the application has no security setup, either on the database or on the API, as this is for demo only.